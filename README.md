# GZCRouter

## 路由模块

路由支持模块，用于支持业务模块间或业务模块内部的路由方式页面跳转、方法互调，以达到各业务模块的解耦。

## 安装

GZCRouter支持[CocoaPods](https://cocoapods.org)，添加下面代码到Podfile中并install即可:

```ruby
pod 'GZCRouter'
```

## 使用

此模块支持两种方式的路由跳转，分别适用不同的情景：

## URL方式路由

URL方式路由的耦合度最低，可以用作跨模块的调用、传参，也可以用作网页跳原生页面的处理，调用双方需要约定好路由地址及传参格式即可，使用代码块作为参数也可以达到获取返回值的效果。

#### 1、默认的地址格式

使用链接地址的方式进行路由跳转，适用跨模块的页面跳转，默认的URL格式为:

```swift
router://host(模块名称)/path(VC类型)?param1=v1&param2=v2
```

调用方式举例：

```swift
/// push方式跳转(需确认当前VC有导航栏，否则无法跳转)
GZCRouter.push(url: "router://GZCRouter_Example/ViewControllerC?ip=1&fp=1.2345&dp=12222.2345&bp=true&name=小明,你好", parameter: params)
/// present方式跳转
GZCRouter.present(url: "router://GZCRouter_Example/ViewControllerC?ip=1&fp=1.2345&dp=12222.2345&bp=true&name=小明,你好", parameter: params)
/// 获取VC自己操作
let vc = GZCRouter.controller(url: "router://GZCRouter_Example/ViewControllerC?ip=1&fp=1.2345&dp=12222.2345&bp=true&name=小明,你好", parameter: params)
```

#### 2、自定义的地址格式

新建实现`GZCRouterHelper`协议的对象并注入Router中，如：

```swift
/// 自定义的Helper对象
public class XXXRouterHelper: NSObject, GZCRouterHelper {
    public func handerOpen(path: String, params: [String : Any]?) -> Bool {
            if path == "url://main" {
                //... 跳转逻辑
              return true
        }
        return false
    }
}

/// 主工程中注入（由于Swift 5 不支持load方法，所以需要手动注入每个模块的路由）
GZCRouter.addHelper(XXXRouterHelper())
```

后续的调用方式与1中相同。

## 协议方式路由

协议方式的路由 通常定义于模块内部，以枚举的方式进行固定形式、参数的页面跳转，然后公开给主工程调用。

优势在于不需要使用字符串来跳转，减少了出错的概率，同时可以枚举的方式进行枚举，使用起来更方便。

例：

```swift
/// 定义枚举
enum RouterPath: GZCRouterPathable {
    case avc
    case bvc(String)
    case rvc(Demo)

    var any: AnyClass {
    switch self {
        case .avc:
            return AVC.self
        case .bvc:
            return BVC.self
        case .rvc:
            return RVC.self
        }
    }

    var params: GZCRouterParameter? {
        switch self {
            case .bvc(let name):
                return ["name":name]
            case .rvc(let demo):
                return ["demo":demo]
            default:
                return nil
        }
    }
}

/// ViewController需要实现 GZCRoutable 协议，完成初始化过程：
extension RVC: GZCRoutable {
    static func initWithParams(params: GZCRouterParameter?) -> UIViewController {
        guard let demo = params?["demo"] as? Demo else {
            fatalError("params is wrong")
        }
        let rvc = RVC(demo: demo)
        return rvc
    }
}

/// 跳转（也有对应的 present 和 controller 方法）
GZCRouter.push(pathable: RouterPath.avc)
```

## 其他任意类型方式的路由

此方式需要定义好路由的路径对象（任意类型都可，推荐枚举），并且在调用模块与实现模块中引入该枚举，进行调用与实现，因此在已经有一个通用的中间层的项目中使用此方式能够在调用时明确地指定参数和类型，避免出错，使用方式如下：

1、创建枚举:

```Swift
// XXX模块
public enum XXXRouterEnum {
    /// 枚举1
    case enum1
    
    /// 枚举2
    /// param1: Int, 参数1，Int类型
    /// param2: Double，参数2，Double类型
    /// param3: [Int]，参数3，Int类型数组
    /// completion: ((Double) -> Void)，参数4，代码块类型，通常用于回调
    case enum2(param1: Int, param2: Double, param3: [Int]?, completion: ((Double) -> Void)?)
}
```

2、实现方在`GZCRouterHelper`协议中实现`handleOpen(path: Any, type: RouterHandleType) -> (Bool, Any?)`方法来进行对应的逻辑处理：

```swift
/// 自定义的Helper对象
public class XXXRouterHelper: NSObject, GZCRouterHelper {
    public func handleOpen(path: Any, type: RouterHandleType) -> (Bool, Any?) {
        guard let path = path as? XXXRouterEnum else { return (false, nil) }
        
        switch path {
        case .enum2(let param1, let param2, let param3,let completion):
                // ... do something
              completion?()
            return (true, nil)
        case .enum1:
            // ... do something
            switch type {
                case .value:
                    return (true, "这是一个返回值")
                default:
                    return (true, nil)
            }
            return (true, nil)
        default:
            return (false, nil)
        }
    }
}
```

3、调用方法:

```swift
/// 直接调用
GZCRouter.handle(path: XXXRouterEnum.enum1, type: .push(true))

/// 获取返回值(需要实现方根据path和type进行处理并返回)
let result = GZCRouter.handle(path: XXXRouterEnum.enum1, type: .value)
```



## Author

郭忠橙, gzhongcheng@qq.com

## License

GZCRouter is available under the MIT license. See the LICENSE file for more info.
