//
//  UIViewController+GZCRouter.swift
//  GZCRouter
//
//  Created by Guo ZhongCheng on 2020/11/1.
//

import UIKit

extension UIViewController {
    
    /// 注入url中的参数参数
    public func initQueryParameters(parameters: [String: String]) {
        let children = Mirror(reflecting: self).children.filter { $0.label != nil }
        for child in children {
            if let key = child.label, let val = parameters[key] {
                let propertyType = type(of: child.value)
                switch propertyType {
                case _ as String.Type, _ as Optional<String>.Type:
                    self.setValue(val, forKey: key)
                case _ as Int.Type:
                    self.setValue(val.intValue, forKey: key)
                case _ as Optional<Int>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Int>类型，改成Int类型")
                    
                case _ as Float.Type:
                    self.setValue(val.floatValue, forKey: key)
                case _ as Optional<Float>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Float>类型，改成Float类型")
                    
                case _ as Double.Type:
                    self.setValue(val.doubleValue, forKey: key)
                case _ as Optional<Double>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Double>类型，改成Double类型")
                    
                case _ as Bool.Type:
                    self.setValue(val.boolValue, forKey: key)
                case _ as Optional<Bool>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Bool>类型，改成Bool类型")
                    
                default:
                    break
                }
            }
        }
    }
    
    /// 注入参数字典
    public func initliazeDicParameters(parameters: [String: Any]) {
        let children = Mirror(reflecting: self).children.filter { $0.label != nil }
        for child in children {
            if let key = child.label, let val = parameters[key] {
                let propertyType = type(of: child.value)
                switch propertyType {
                case _ as Optional<Int>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Int>类型，改成Int类型")
                case _ as Optional<Float>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Float>类型，改成Float类型")
                case _ as Optional<Double>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Double>类型，改成Double类型")
                case _ as Optional<Bool>.Type:
                    assert(false, "GZCRouter --> 参数不支持Optional<Bool>类型，改成Bool类型")
                default:
                    break
                }
                self.setValue(val, forKey: key)
            }
        }
    }
}
