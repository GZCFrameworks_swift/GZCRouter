//
//  URL+GZCRouter.swift
//  GZCRouter
//
//  Created by Guo ZhongCheng on 2020/11/1.
//

import Foundation

extension URL {
    
    /// 解析URL链接中的参数
    public var routerQueryParameters: [String : String] {
        get {
            var dic = [String: String]();
            
            guard let queryStr = query else {
                return [:]
            }
            let queryArray = (queryStr.components(separatedBy: "&")) as Array<String>
            
            for index in 0 ..< queryArray.count {
                let queryComponent = queryArray[index]
                let compArr = queryComponent.components(separatedBy: "=") as Array<String>
                if compArr.count >= 2 {
                    let key = compArr[0]
                    let val = compArr[1].removingPercentEncoding
                    dic.updateValue(val ?? "", forKey: key)
                }
            }
            return dic
        }
    }
}
