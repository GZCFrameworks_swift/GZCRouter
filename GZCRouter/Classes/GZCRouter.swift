//
//  GZCRouter.swift
//  GZCRouter
//
//  Created by Guo ZhongCheng on 2020/11/1.
//

import UIKit

// MARK:- 路由协议
/// 自定义路由协议（通常定义于模块内部，以枚举的方式进行固定形式、参数的页面跳转，然后公开给主工程调用）
public protocol GZCRouterPathable {
    /// **返回的Class对象必须实现GZCRoutable协议**
    var any: AnyClass { get }
    /// 设置的参数
    var params: GZCRouterParameter? { get }
}


/// 自定义路由表对象
public protocol GZCRoutable {
  /**
   类的初始化方法
   - params 传参字典
   */
  static func initWithParams(params: GZCRouterParameter?) -> UIViewController
}

/// 路由参数
public typealias  GZCRouterParameter = [String: Any]


public struct GZCRouter {
// MARK:- 自定义协议方式路由
    
    /// push方式打开视图
    /// - Parameters:
    ///   - path: 实现了`GZCRouterPathable`协议的path对象，用于创建跳转页面
    ///   - animated: 是否带动画
    public static func push(pathable path: GZCRouterPathable, animated: Bool = true) {
        Router.shared.push(pathable: path, animated: animated)
    }
    
    /// present方式打开视图
    /// - Parameters:
    ///   - path: 实现了`GZCRouterPathable`协议的path对象，用于创建跳转页面
    ///   - animated: 是否带动画
    ///   - presentComplete: present完成时的回调
    public static func present(pathable path: GZCRouterPathable, animated: Bool = true , presentComplete: (()->Void)? = nil) {
        Router.shared.present(pathable: path, animated: animated, presentComplete: presentComplete)
    }
    
// MARK:- URL方式路由
    /// push方式打开视图（UIViewController, web page, other app）
    ///
    /// - Parameters:
    ///   - url: 可以是默认的格式：router://host(TargetName)/path(controller)?param1=v1&param2=v2
    ///          (也可以是自己定的url字符串，这样需要自己实现helper协议来跳转)
    ///   - parameter: url里面不能包含的参数，从这里传
    public static func push(url: String, parameter: GZCRouterParameter? = nil) {
        Router.shared.push(url: url, parameter: parameter)
    }


    /// present方式打开视图（UIViewController, web page）
    ///
    /// - Parameters:
    ///   - url: router://host(TargetName)/path(controller)?param1=v1&param2=v2
    ///   - parameter:  url里面不能包含的参数，从这里传
    public static func present(url: String, parameter: GZCRouterParameter? = nil) {
        Router.shared.present(url: url, parameter: parameter)
    }

    /// 通过urlString获取UIViewController实例
    ///
    /// - Parameters:
    ///   - url: router://host(TargetName)/path(controller)?param1=v1&param2=v2
    ///   - parameter:  url里面不能包含的参数，从这里传
    public static func controller(from urlString: String, parameter: GZCRouterParameter? = nil) -> UIViewController? {
        return Router.shared.controller(from: urlString, parameter: parameter)
    }
    
    public static func controller(pathable path: GZCRouterPathable, parameter: GZCRouterParameter? = nil) -> UIViewController? {
        return Router.shared.controller(pathable: path, parameter: parameter)
    }

    /// 某些特殊情况DefaultHelper不能满足项目的个性化需求
    /// 这时可以通过该方法注入自己的查找的Helper类，
    ///
    /// - Parameters:
    ///   - helper: 用来查找navigationController和topViewController的帮助类（不设置的话使用 DefaultHelper）
    public static func addHelper(_ helper: GZCRouterHelper) {
        Router.shared.helper.append(helper)
    }

    /// 如果要支持http的url需要设置WebViewController的工厂类
    ///
    /// - Parameters:
    /// - factory: 用来生成Web容器控制器的工厂类（需要实现WebVCFactory协议）
    public static func setWebVCFactory(factory: WebVCFactory) {
        Router.shared.webVCFactory = factory
    }

    /// 拨打电话
    /// - Parameter phone: 电话号码
    public static func openTel(_ phone:String) {
        if let url = URL(string: "tel://\(phone)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}


// MARK:- 新增对协议类型路由的支持
extension GZCRouter {
    
    @discardableResult
    public static func handle(path: Any, type: RouterHandleType) -> Any?  {
        return Router.shared.open(path: path, type: type)
    }
    
}
