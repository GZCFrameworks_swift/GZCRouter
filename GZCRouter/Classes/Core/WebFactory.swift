//
//  WebFactory.swift
//  GZCRouter
//
//  Created by Guo ZhongCheng on 2020/11/1.
//

import UIKit

/// 跳转网页的对象协议
public protocol WebVCFactory {
    func createWebVC(with urlString: String, parameter: [String: Any]) -> UIViewController
}
