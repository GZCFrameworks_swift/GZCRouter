//
//  Router.swift
//  GZCRouter
//
//  Created by Guo ZhongCheng on 2020/11/1.
//

@_exported import GZCExtends

import UIKit
import GZCExtends

public class Router {
    var helper: [GZCRouterHelper] = []
    var webVCFactory: WebVCFactory?
 
    static let shared: Router = {
       return Router()
    }()
    
    func push(url: String, parameter: [String: Any]?) {
       self.open(urlString: url, parameter: parameter, modal: false)
    }
    
    func present(url: String, parameter: [String: Any]?) {
        self.open(urlString: url, parameter: parameter, modal: true)
    }
    
    // MARK: - Private
    func open(urlString: String, parameter: [String: Any]?, modal: Bool) {
        for h in self.helper {
            if h.handerOpen(path: urlString, params: parameter) {
                return
            }
        }
        if let controller = self.controller(from: urlString, parameter: parameter) {
            controller.hidesBottomBarWhenPushed = true
            controller.modalPresentationStyle = .fullScreen
            if modal || UIApplication.topNavigationController == nil {
                UIApplication.topViewController?.present(controller, animated: true, completion: nil)
            } else {
                UIApplication.topNavigationController?.modalPresentationStyle = .fullScreen
                UIApplication.topNavigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func controller(from urlString: String, parameter: [String: Any]? = nil) -> UIViewController? {
        if let url = urlString.asURL(), let target = url.host {
            if let scheme = url.scheme,
                (scheme == "http" || scheme == "https") {
                // Web View Controller
                let webController: UIViewController? = self.webVCFactory?.createWebVC(with: urlString, parameter: parameter ?? [:])
                return webController
            } else {
                // controller
                let path = url.path.replacingOccurrences(of: "/", with: "")
                let className = "\(target).\(path)"
                let cls: AnyClass? = NSClassFromString(className)
                if let controller = cls as? UIViewController.Type {
                    let viewController: UIViewController = controller.init()
                    
                    // parameter
                    viewController.initQueryParameters(parameters: url.routerQueryParameters)
                    if let dicParameters = parameter {
                        viewController.initliazeDicParameters(parameters: dicParameters)
                    }
                    
                    return viewController
                } else {
                    print(false, "GZCRouter ---> \(className) 必须是UIViewController类型或者其子类型")
                }
            }
        } else {
            print(false, "GZCRouter ---> url.host不能为空，必须为类所在的Target Name")
        }
        return nil
    }
    
    // MARK:- 自定义协议方式路由
    func push(pathable path:GZCRouterPathable, animated: Bool = true) {
        self.open(pathable: path, present: false, animated: animated, presentComplete: nil)
    }

    func present(pathable path:GZCRouterPathable, animated: Bool = true , presentComplete: (()->Void)? = nil) {
        self.open(pathable: path, present: true, animated: animated, presentComplete: presentComplete)
    }
    
    func open(pathable path:GZCRouterPathable , present: Bool = false , animated: Bool = true , presentComplete: (() -> Void)? = nil){
        if let cls = path.any as? GZCRoutable.Type {
            let vc = cls.initWithParams(params: path.params)
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
            if present || UIApplication.topNavigationController == nil {
                UIApplication.topViewController?.present(vc, animated: animated, completion: presentComplete)
            } else {
                UIApplication.topNavigationController?.modalPresentationStyle = .fullScreen
                UIApplication.topNavigationController?.pushViewController(vc, animated: animated)
            }
        }
    }
    
    func controller(pathable path:GZCRouterPathable, parameter: [String: Any]? = nil) -> UIViewController? {
        if let cls = path.any as? GZCRoutable.Type {
            let vc = cls.initWithParams(params: path.params)
            return vc
        }
        return nil
    }
    
}


// MARK: - 对协议类型路由的支持
extension Router {
    
    func open(path: Any, type: RouterHandleType) -> Any? {
        for item in helper {
            let response = item.handleOpen(path: path, type: type)
            if  response.0 == true {
                return response.1
            }
        }
        return nil
    }
}
