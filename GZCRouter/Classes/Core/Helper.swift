//
//  Helper.swift
//  GZCRouter
//
//  Created by Guo ZhongCheng on 2020/11/1.
//

import UIKit

/// Helper协议
public protocol GZCRouterHelper {
    /// 处理字符串带参数类型的路由
    /// 参数：
    /// - path: 路由路径（字符串类型）
    /// - params: 参数（传递的参数字典）
    /// 返回值(Bool):  是否已经处理该路由（返回true则后续不继续处理，返回false则会继续处理跳转逻辑）
    func handerOpen(path: String, params: [String: Any]?) -> Bool
    
    /// 处理Any类型的路由
    /// 参数：
    /// - path:  路由的路径对象（通常为枚举值）
    /// - type:  路由的处理类型（通常为同一个路径使用不同处理方式时用到）
    /// 返回值（元组）：
    /// - Bool: 是否已经处理该路由（返回true则后续不继续处理，返回false则会继续处理跳转逻辑）
    /// - Any: 返回给调用方的返回值
    func handleOpen(path: Any, type: RouterHandleType) -> (Bool, Any?)
}


// 添加默认实现
public extension GZCRouterHelper {
    
    func handerOpen(path: String, params: [String: Any]?) -> Bool {
        return false
    }
    
    func handleOpen(path: Any, type: RouterHandleType) -> (Bool, Any?) {
        return (false, nil)
    }
}


/// 处理类型（仅为标识，具体处理还需实现方自行操作）
public enum RouterHandleType {
    // 不做特殊处理
    case none
    // push动画
    case push(_ animated: Bool = true)
    // present动画
    case present(_ animated: Bool = true , complete: (()->Void)? = nil)
    // 将VC作为目标VC的child，添加到VC上, 通常会返回VC本身，供外部操作
    case child(_ toVC: UIViewController)
    // 获取返回值
    case value
}
