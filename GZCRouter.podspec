#
# Be sure to run `pod lib lint GZCRouter.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GZCRouter'
  s.version          = '0.1.0'
  s.summary          = '路由模块'

  s.description      = <<-DESC
  路由支持模块，用于支持业务模块间或业务模块内部的路由方式页面跳转、方法互调，以达到各业务模块的解耦。
                       DESC

  s.homepage         = 'https://gitlab.com/GZCFrameworks_swift/GZCRouter'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guo ZhongCheng' => 'gzhongcheng@qq.com' }
  s.source           = { :git => 'https://gitlab.com/GZCFrameworks_swift/GZCRouter.git', :tag => s.version.to_s }
  
  s.ios.deployment_target = '9.0'
  s.swift_version = "5.1"

  s.source_files = 'GZCRouter/Classes/**/*'
  
  s.dependency 'GZCExtends/UIKit'
end
