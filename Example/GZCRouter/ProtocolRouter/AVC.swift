//
//  AVC.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import GZCRouter

class AVC: UIViewController, GZCRoutable{
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.red
    }

    static func initWithParams(params: GZCRouterParameter?) -> UIViewController {
        return AVC()
    }
}
