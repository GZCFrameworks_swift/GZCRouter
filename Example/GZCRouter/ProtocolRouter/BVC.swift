//
//  BVC.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import GZCRouter

class BVC: UIViewController, GZCRoutable{
    let name: String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blue
    }
    
    init(name: String) {
        self.name = name
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    static func initWithParams(params: GZCRouterParameter?) -> UIViewController {
        guard let name = params?["name"] as? String else {
            fatalError("params is wrong")
        }
        let bvc = BVC(name: name)
        return bvc
    }
}

