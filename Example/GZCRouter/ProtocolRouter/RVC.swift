//
//  RVC.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import GZCRouter

struct Demo {
    var name: String
    var id: Int
}

class RVC: UIViewController {
    let demo:Demo
    
    init(demo:Demo) {
        self.demo = demo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = demo.name
    }
}

extension RVC: GZCRoutable {
    static func initWithParams(params: GZCRouterParameter?) -> UIViewController {
        guard let demo = params?["demo"] as? Demo else {
            fatalError("params is wrong")
        }
        let rvc = RVC(demo: demo)
        return rvc
    }
}
