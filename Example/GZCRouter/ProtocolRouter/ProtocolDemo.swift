//
//  ProtocolDemo.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import GZCRouter

enum RouterPath: GZCRouterPathable {
    case avc
    case bvc(String)
    case rvc(Demo)

    var any: AnyClass {
    switch self {
        case .avc:
            return AVC.self
        case .bvc:
            return BVC.self
        case .rvc:
            return RVC.self
        }
    }

    var params: GZCRouterParameter? {
        switch self {
            case .bvc(let name):
                return ["name":name]
            case .rvc(let demo):
                return ["demo":demo]
            default:
                return nil
        }
    }
}


class ProtocolDemo: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "协议方式路由"
        tableView.rowHeight = 50
    }

    @IBAction func nomalPush(_ sender: Any) {
        GZCRouter.push(pathable: RouterPath.avc)
    }


    @IBAction func pushWithParams(_ sender: Any) {
    let demo = Demo(name: "RVC title", id: 1)
        GZCRouter.push(pathable: RouterPath.rvc(demo))
    }

    @IBAction func xpresent(_ sender: Any) {
        GZCRouter.present(pathable: RouterPath.bvc("BVC title"))
    }
}

extension ProtocolDemo: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DemoCell
        return cell
    }
}
