//
//  DemoCell.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import GZCRouter

class DemoCell: UITableViewCell {
    @IBOutlet weak var leftLabel: UILabel!
    override func awakeFromNib() {
    super.awakeFromNib()

    let tap = UITapGestureRecognizer(target: self, action: #selector(push))
        contentView.addGestureRecognizer(tap)
        contentView.isUserInteractionEnabled = true
    }

    @objc func push(){
        GZCRouter.push(pathable: RouterPath.avc)
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
}
