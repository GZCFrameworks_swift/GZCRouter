//
//  URLRouterDemo.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import GZCRouter

class URLRouterDemo: UIViewController {
    
    let label: UILabel = {
        let lab = UILabel()
        lab.text = "A"
        lab.textAlignment = .center
        lab.textColor = UIColor.red
        lab.font = UIFont.systemFont(ofSize: 38)
        
        return lab
    }()
    let nextBButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("ViewControllerB", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.addTarget(self, action: #selector(onNextBTouch), for: .touchUpInside)
        
        return btn
    }()
    
    let nextCButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("ViewControllerC", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.addTarget(self, action: #selector(onNextCTouch), for: .touchUpInside)
        
        return btn
    }()
    
    let nextDButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("ViewControllerD", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.addTarget(self, action: #selector(onNextDTouch), for: .touchUpInside)
        
        return btn
    }()
    
    let nextWebButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("WebViewController", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.addTarget(self, action: #selector(onNextWebTouch), for: .touchUpInside)
        
        return btn
    }()
    
    let nextCustomButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitle("自定义路由地址", for: .normal)
        btn.setTitleColor(UIColor.blue, for: .normal)
        btn.addTarget(self, action: #selector(onNextCustomTouch), for: .touchUpInside)
        
        return btn
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "url方式路由"
        
        view.addSubview(label)
        let h = view.bounds.size.height, w = view.bounds.size.width
        view.addSubview(nextBButton)
        view.addSubview(nextCButton)
        view.addSubview(nextDButton)
        view.addSubview(nextWebButton)
        view.addSubview(nextCustomButton)
        nextDButton.frame = CGRect(x: 0, y: h-100, width: w, height: 50)
        nextCButton.frame = CGRect(x: 0, y: h-150, width: w, height: 50)
        nextBButton.frame = CGRect(x: 0, y: h-200, width: w, height: 50)
        nextWebButton.frame = CGRect(x: 0, y: h-250, width: w, height: 50)
        nextCustomButton.frame = CGRect(x: 0, y: h-300, width: w, height: 50)
        label.frame = CGRect(x: 0, y: h-400, width: w, height: 50)
        
        // 支持网页
        GZCRouter.setWebVCFactory(factory: WebViewControllerFactory())
    }

    
    // MARK: - Events
    @objc func onNextBTouch() {
        GZCRouter.push(url: "router://GZCRouter_Example/ViewControllerB")
    }
    @objc func onNextCTouch() {
        let params:[String:Any] = ["image": UIImage(named: "meinv") as Any]
        GZCRouter.push(url: "router://GZCRouter_Example/ViewControllerC?ip=1&fp=1.2345&dp=12222.2345&bp=true&name=小明,你好", parameter: params)
    }
    @objc func onNextDTouch() {
        GZCRouter.push(url: "router://GZCRouter_Example/ViewControllerD")
    }
    
    @objc func onNextWebTouch() {
        GZCRouter.push(url: "https://www.baidu.com", parameter: ["title": "测试网页传参数"])
    }

    @objc func onNextCustomTouch() {
        GZCRouter.push(url: "自定义的路由地址")
    }
}

