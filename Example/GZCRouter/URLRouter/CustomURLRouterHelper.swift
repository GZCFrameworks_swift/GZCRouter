//
//  CustomURLRouterHelper.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import GZCRouter

public class CustomURLRouterHelper: NSObject, GZCRouterHelper {
    public func handerOpen(path: String, params: [String : Any]?) -> Bool {
        if path == "自定义的路由地址" {
            UIAlertController(title: "自定义路由事件").show()
            return true
        }
        return false
    }
}
