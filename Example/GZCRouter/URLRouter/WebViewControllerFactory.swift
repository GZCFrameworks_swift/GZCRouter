//
//  WebViewControllerFactory.swift
//  GZCRouter_Example
//
//  Created by Guo ZhongCheng on 2020/11/1.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import GZCRouter

class WebViewControllerFactory: WebVCFactory {
    func createWebVC(with urlString: String, parameter: [String : Any]) -> UIViewController {
        let webVC = WebViewController()
            webVC.urlString = urlString
        webVC.title = parameter["title"] as? String
        
        return webVC
    }
    
}
