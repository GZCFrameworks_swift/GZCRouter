# GZCExtends

扩展库

集成了SwifterSwift中的相关扩展，并增加了一些自己用的扩展方法，如：
UILabel、UIButton常用属性初始化方法
图片缩放、网络图片加载缓存、图片圆角（支持单个和多个角圆角）
主线程延迟执行
获取状态栏、导航栏、屏幕宽高等

## 安装

GZCExtends 支持 [CocoaPods](https://cocoapods.org). 安装
在 Podfile 中添加引用:

```
pod 'GZCExtends'
```

## Author

Guo ZhongCheng, gzhongcheng@qq.com

## License

GZCExtends is available under the MIT license. See the LICENSE file for more info.
