//
//  UIButtonExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

@_exported import SwifterSwift
import SwifterSwift

// MARK: - Methods
public extension UIButton {
    // 初始化
    /// 快速创建并设置常用属性
    /// - Parameters:
    ///   - image: 图片
    ///   - title: 标题
    ///   - font: 字体
    ///   - style: 系统定义好的字体风格（设置后font失效）
    ///   - titleColor: 标题颜色
    ///   - backgroundImage: 背景图片（设置后backgroundColor失效）
    ///   - backgroundColor: 背景颜色
    ///   - cornerRadius: 圆角
    convenience init(image: UIImage? = nil, title: String? = nil, font: UIFont? = UIFont.systemFont(ofSize: 14), style: UIFont.TextStyle? = nil, titleColor: UIColor? = .black, backgroundImage: UIImage? = nil, backgroundColor: UIColor? = nil, cornerRadius: CGFloat? = nil) {
        self.init(type: .custom)
        if let image = image {
            setImageForAllStates(image)
        }
        if let title = title {
            setTitleForAllStates(title)
        }
        if let style = style {
            titleLabel?.font = UIFont.preferredFont(forTextStyle: style)
        } else
        if let font = font {
            titleLabel?.font = font
        }
        if let textColor = titleColor {
            setTitleColorForAllStates(textColor)
        }
        if let bgImage = backgroundImage {
            setBackgroundImage(bgImage, for: .normal)
        }
        if let bgColor = backgroundColor {
            let bgImage = UIImage(color: bgColor, size: CGSize(width: 10, height: 10))
            setBackgroundImage(bgImage, for: .normal)
        }
        if let radius = cornerRadius {
            layer.cornerRadius = radius
            clipsToBounds = true
        }
    }
    
    /// 图文居中
    ///
    /// - Parameter imageAboveText: 图片是否在文字上方
    /// - Parameter spacing: 图文的间距
    func centerTextAndImage(imageAboveText: Bool = false, spacing: CGFloat) {
        if imageAboveText {
            guard
                let imageSize = imageView?.image?.size,
                let text = titleLabel?.text,
                let font = titleLabel?.font
                else { return }

            let titleSize = text.size(withAttributes: [.font: font])

            let titleOffset = -(imageSize.height + spacing)
            titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: titleOffset, right: 0.0)

            let imageOffset = -(titleSize.height + spacing)
            imageEdgeInsets = UIEdgeInsets(top: imageOffset, left: 0.0, bottom: 0.0, right: -titleSize.width)

            let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0
            contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
        } else {
            let insetAmount = spacing / 2
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
            contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
        }
    }

    
    /// 图片位置枚举
    enum ButtonEdgeInsetsStyle {
        case ImageTop //图片在上
        case ImageLeft //图片在左
        case ImageBottom //图片在下
        case ImageRight //图片在右
    }
    
    /// 按钮图文位置设置，需要在正确设置frame后调用
    /// - Parameters:
    ///   - style: 图片位置枚举
    ///   - space: 图文间距
    func layoutButtonWithEdgInsetStyle(_ style: ButtonEdgeInsetsStyle,_ space:CGFloat){
        //获取image宽高
        let imageW = self.imageView?.frame.size.width
        let imageH = self.imageView?.frame.size.height
        //获取label宽高
        var lableW = self.titleLabel?.intrinsicContentSize.width
        let lableH = self.titleLabel?.intrinsicContentSize.height
        
        var imageEdgeInsets:UIEdgeInsets = .zero
        var lableEdgeInsets:UIEdgeInsets = .zero
        if self.frame.size.width <= lableW! { //如果按钮文字超出按钮大小，文字宽为按钮大小
            lableW = self.frame.size.width
        }
        //根据传入的 style 及 space 确定 imageEdgeInsets和labelEdgeInsets的值
        switch style {
        case .ImageTop:
            imageEdgeInsets = UIEdgeInsets(top: 0.0 - lableH! - space/2.0, left: 0, bottom: 0, right: 0.0 - lableW!)
            lableEdgeInsets = UIEdgeInsets(top: 0, left: 0.0 - imageW!, bottom: 0.0 - imageH! - space/2.0, right: 0)
        case .ImageLeft:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0.0 - space/2.0, bottom: 0, right: space/2.0)
            lableEdgeInsets = UIEdgeInsets(top: 0, left: space/2.0, bottom: 0, right: 0.0 - space/2.0)
        case .ImageBottom:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0.0 - lableH! - space/2.0, right: 0.0 - lableW!)
            lableEdgeInsets = UIEdgeInsets(top: 0.0 - imageH! - space/2.0, left: 0.0 - imageW!, bottom: 0, right: 0)
        case .ImageRight:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: lableW! + space/2.0, bottom: 0, right: 0.0 - lableW! - space/2.0)
            lableEdgeInsets = UIEdgeInsets(top: 0, left: 0.0 - imageW! - space/2.0, bottom: 0, right: imageW! + space/2.0)
        }
        //赋值
        self.titleEdgeInsets = lableEdgeInsets
        self.imageEdgeInsets = imageEdgeInsets
    }
}
