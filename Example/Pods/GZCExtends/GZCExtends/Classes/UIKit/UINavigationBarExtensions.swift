//
//  UINavigationBarExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import SwifterSwift

// MARK: - Methods
public extension UINavigationBar {
    /// 设置默认风格
    /// - Parameters:
    ///   - backgroundColor: 背景色
    ///   - textColor: 文字颜色
    ///   - titleFont: 标题字体
    ///   - hideLine: 是否隐藏底部线条（默认为true）
    ///   - backImage: 返回按钮图片
    static func setStyles(backgroundColor: UIColor, textColor: UIColor, titleFont: UIFont, hideLine: Bool = true, backImage: UIImage?) {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().backgroundColor = backgroundColor
        UINavigationBar.appearance().barTintColor = backgroundColor
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().tintColor = textColor
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: textColor,.font: titleFont]
        if backImage != nil {
            UINavigationBar.appearance().backIndicatorImage = backImage
            UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        }
        if hideLine {
            UINavigationBar.appearance().shadowImage = UIImage()
        }
    }
}
