//
//  UIViewControllerExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import UIKit
import SwifterSwift

@objc public extension UIViewController {
    
    /// 设置导航栏的返回按钮
    func setNavigationBackButton() {
        guard UINavigationBar.appearance().backIndicatorImage != nil else {
            return
        }
        // 返回按钮
        let backButton = UIButton(type: .custom)
        // 给按钮设置返回箭头图片
        backButton.setImage(UINavigationBar.appearance().backIndicatorImage, for: .normal)
        // 设置frame
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -UINavigationBar.appearance().backIndicatorImage!.size.width - 3,bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(popBack), for: .touchUpInside)
        // 自定义导航栏的UIBarButtonItem类型的按钮
        let backView = UIBarButtonItem(customView: backButton)
        // 重要方法，用来调整自定义返回view距离左边的距离
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        barButtonItem.width = 3
        navigationItem.leftBarButtonItems = [barButtonItem, backView]
//        navigationItem.leftBarButtonItem = backView
    }
    
    @objc open func popBack() {
        self.navigationController?.popViewController()
    }
}

// MARK: - 属性
public extension UIViewController {
    /// 状态栏高度
    var statusBarHeight: CGFloat {
        get {
            return UIApplication.shared.statusBarFrame.size.height
        }
    }
    
    /// 导航栏高度
    var navigationBarHeight: CGFloat {
        get {
            guard let navigationController = self.navigationController else {
                return 0
            }
            let navigationBarHeight = navigationController.navigationBar.frame.size.height
            return navigationBarHeight
        }
    }
    
    /// 状态栏+导航栏高度
    var statusAndNavigationBarHeight: CGFloat {
        get {
            return statusBarHeight + navigationBarHeight
        }
    }
    
    /// 底部安全距离
    var bottomSafeAreaHeight: CGFloat {
        get {
            return statusBarHeight > 20 ? 34 : 0
        }
    }
    
    /// 屏幕size
    static var screenSize: CGSize {
        get {
            return UIScreen.main.bounds.size
        }
    }
    var screenSize: CGSize {
        get {
            return UIScreen.main.bounds.size
        }
    }
    
    /// 屏幕宽度
    static var screenWidth: CGFloat {
        get {
            return screenSize.width
        }
    }
    var screenWidth: CGFloat {
        get {
            return screenSize.width
        }
    }
    
    /// 屏幕高度
    static var screenHeight: CGFloat {
        get {
            return screenSize.height
        }
    }
    var screenHeight: CGFloat {
        get {
            return screenSize.height
        }
    }

}
