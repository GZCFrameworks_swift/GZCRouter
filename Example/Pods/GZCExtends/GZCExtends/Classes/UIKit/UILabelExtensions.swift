//
//  UILabelExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import UIKit
import SwifterSwift

// MARK: - Methods
public extension UILabel {
    
    // 初始化
    /// 文字内容等创建
    /// - Parameters:
    ///   - text: 文本内容
    ///   - font: 字体
    ///   - style: UIFont.TextStyle，设置了style后font失效
    ///   - textColor: 字体颜色
    ///   - alignment: 对齐方式
    ///   - numberOfLines: 行数
    convenience init(text: String? = nil, font: UIFont? = UIFont.systemFont(ofSize: 14), style: UIFont.TextStyle? = nil, textColor: UIColor? = .black, alignment: NSTextAlignment? = .center, numberOfLines: Int? = 1) {
        self.init()
        self.text = text
        if let style = style {
            self.font = UIFont.preferredFont(forTextStyle: style)
        } else
        if let font = font {
            self.font = font
        }
        self.textColor = textColor
        self.textAlignment = alignment ?? .center
        self.numberOfLines = numberOfLines ?? 1
    }
}
