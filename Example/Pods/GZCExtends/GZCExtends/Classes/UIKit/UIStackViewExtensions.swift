//
//  UIStackViewExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import SwifterSwift

@available(iOS 9.0, *)
public extension UIStackView {

    /// 移除所有子View
    func removeAllArrangedSubviews() {
        for view in arrangedSubviews {
            view.removeFromSuperview()
        }
    }

}
