//
//  UIViewExtensions.swift
//  GZCExtends
//
//  Created by Guo ZhongCheng on 2020/10/20.
//

import UIKit
import SwifterSwift

// MARK: - 属性
public extension UIView {
    /// 状态栏高度
    static var statusBarHeight:CGFloat {
        get{
            return UIApplication.shared.statusBarFrame.size.height
        }
    }
    
    /// 底部安全距离
    static var bottomSafeAreaHeight: CGFloat {
        get {
            return statusBarHeight > 20 ? 34 : 0
        }
    }
    
    /// 屏幕size
    static var screenSize: CGSize {
        get {
            return UIScreen.main.bounds.size
        }
    }
    var screenSize: CGSize {
        get {
            return UIScreen.main.bounds.size
        }
    }
    
    /// 屏幕宽度
    static var screenWidth: CGFloat {
        get {
            return screenSize.width
        }
    }
    var screenWidth: CGFloat {
        get {
            return screenSize.width
        }
    }
    
    /// 屏幕高度
    static var screenHeight: CGFloat {
        get {
            return screenSize.height
        }
    }
    var screenHeight: CGFloat {
        get {
            return screenSize.height
        }
    }
}
